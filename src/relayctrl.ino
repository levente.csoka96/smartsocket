/**

   Copyright 2019 Levente Csóka

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation the
   rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is furnished to
   do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all copies
   or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
   PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
   FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE.

**/

#include <SoftwareSerial.h>

#define relayPin 4
#define timeInterval 60000

bool state = false;
unsigned long lastSwitch = 0;
char c = ' ';
SoftwareSerial BTserial(8, 6);

void checkTimer() {
  if (millis() - lastSwitch >= timeInterval) {
    state = !state;
    lastSwitch = millis();
  }
}

void setup() {
  Serial.begin(9600);
  BTserial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(relayPin, OUTPUT);
}

void loop() {
  checkTimer();
  //digitalWrite(LED_BUILTIN, (state ? HIGH : LOW));
  //digitalWrite(relayPin, (state ? HIGH : LOW));
  if (BTserial.available())
    {  
        String str = BTserial.readString();
        if(str.equals("on"))
        {
          digitalWrite(relayPin, HIGH);
          digitalWrite(LED_BUILTIN, HIGH);
        }
        if(str.equals("off"))
        {
          digitalWrite(relayPin, LOW);
          digitalWrite(LED_BUILTIN, LOW);
        }
        
        //Serial.writeString(str);
    }
}
