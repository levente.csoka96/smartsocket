# SmartSocket

SmartSocket is an Arduino based project for controlling power wirelessly.

## Installation

Use the [Arduino IDE](https://www.arduino.cc/en/main/software) to edit or upload the project.

## Usage

Send the message "on" or "off" through BLE to turn on or off the power.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## License
[MIT](https://choosealicense.com/licenses/mit/)
